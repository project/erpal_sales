<?php
/**
 * @file
 * erpal_sales.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function erpal_sales_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|erpal_sale|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'erpal_sale';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'erpal_forms';
  $ds_layout->settings = array(
    'hide_empty_regions' => 1,
    'hide_sidebars' => FALSE,
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_contractor_ref',
        2 => 'field_customer_ref',
        3 => 'field_sale_customer_address',
        4 => 'field_sale_status',
        5 => 'field_sale_items',
      ),
      'right' => array(
        0 => 'field_sale_transaction_number',
        1 => 'field_activity_ref',
        2 => 'field_sale_quotation_date',
        3 => 'field_sale_valid_until',
        4 => 'field_asset_files',
      ),
      'advanced' => array(
        0 => 'field_sale_offer_notes',
        1 => 'field_sale_confirmation_notes',
        2 => 'field_skonto',
        3 => '_add_existing_field',
      ),
      'hidden' => array(
        0 => 'path',
        1 => 'field_sale_total_incl_vat',
        2 => 'field_total_excl_vat',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_contractor_ref' => 'left',
      'field_customer_ref' => 'left',
      'field_sale_customer_address' => 'left',
      'field_sale_status' => 'left',
      'field_sale_items' => 'left',
      'field_sale_transaction_number' => 'right',
      'field_activity_ref' => 'right',
      'field_sale_quotation_date' => 'right',
      'field_sale_valid_until' => 'right',
      'field_asset_files' => 'right',
      'field_sale_offer_notes' => 'advanced',
      'field_sale_confirmation_notes' => 'advanced',
      'field_skonto' => 'advanced',
      '_add_existing_field' => 'advanced',
      'path' => 'hidden',
      'field_sale_total_incl_vat' => 'hidden',
      'field_total_excl_vat' => 'hidden',
    ),
    'classes' => array(),
  );
  $export['node|erpal_sale|form'] = $ds_layout;

  return $export;
}
