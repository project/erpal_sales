<?php
/**
 * @file
 * erpal_sales.features.field_conditional_state.inc
 */

/**
 * Implements hook_field_conditional_state_defaults().
 */
function erpal_sales_field_conditional_state_defaults() {
  $items = array();

  $items["erpal_sale:field_activity_ref:node:field_sale_status:required:offer_accepted-in_production-delivered"] = array(
    'type' => 'node',
    'bundle' => 'erpal_sale',
    'field_name' => 'field_activity_ref',
    'control_field' => 'field_sale_status',
    'state' => 'required',
    'condition_type' => 'or',
    'trigger_values' => array(
      'offer_accepted' => 'offer_accepted',
      'in_production' => 'in_production',
      'delivered' => 'delivered',
    ),
  );

  $items["erpal_sale:field_sale_confirmation_notes:node:field_sale_status:visible:offer_accepted"] = array(
    'type' => 'node',
    'bundle' => 'erpal_sale',
    'field_name' => 'field_sale_confirmation_notes',
    'control_field' => 'field_sale_status',
    'state' => 'visible',
    'condition_type' => 'or',
    'trigger_values' => array(
      'offer_accepted' => 'offer_accepted',
    ),
  );

  $items["erpal_sale:field_sale_offer_notes:node:field_sale_status:visible:offer_submitted"] = array(
    'type' => 'node',
    'bundle' => 'erpal_sale',
    'field_name' => 'field_sale_offer_notes',
    'control_field' => 'field_sale_status',
    'state' => 'visible',
    'condition_type' => 'or',
    'trigger_values' => array(
      'offer_submitted' => 'offer_submitted',
    ),
  );

  $items["erpal_sale:field_sale_valid_until:node:field_sale_status:visible:customer_request-offer_submitted"] = array(
    'type' => 'node',
    'bundle' => 'erpal_sale',
    'field_name' => 'field_sale_valid_until',
    'control_field' => 'field_sale_status',
    'state' => 'visible',
    'condition_type' => 'or',
    'trigger_values' => array(
      'customer_request' => 'customer_request',
      'offer_submitted' => 'offer_submitted',
    ),
  );

  $items["field_sale_items:field_sale_items_description:field_collection_item:field_sale_items_item_type:required:proj-other"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items',
    'field_name' => 'field_sale_items_description',
    'control_field' => 'field_sale_items_item_type',
    'state' => 'required',
    'condition_type' => 'or',
    'trigger_values' => array(
      'proj' => 'proj',
      'other' => 'other',
    ),
  );

  $items["field_sale_items:field_sale_items_discount_amount:field_collection_item:field_sale_items_discount_type:disabled:abs-perc"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items',
    'field_name' => 'field_sale_items_discount_amount',
    'control_field' => 'field_sale_items_discount_type',
    'state' => 'disabled',
    'condition_type' => 'or',
    'trigger_values' => array(
      'abs' => 'abs',
      'perc' => 'perc',
    ),
  );

  $items["field_sale_items:field_sale_items_discount_amount:field_collection_item:field_sale_items_discount_type:disabled:abs-perc"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items',
    'field_name' => 'field_sale_items_discount_amount',
    'control_field' => 'field_sale_items_discount_type',
    'state' => 'disabled',
    'condition_type' => 'or',
    'trigger_values' => array(
      'abs' => 'abs',
      'perc' => 'perc',
    ),
  );

  $items["field_sale_items:field_sale_items_discount_amount:field_collection_item:field_sale_items_discount_type:required:abs-perc"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items',
    'field_name' => 'field_sale_items_discount_amount',
    'control_field' => 'field_sale_items_discount_type',
    'state' => 'required',
    'condition_type' => 'or',
    'trigger_values' => array(
      'abs' => 'abs',
      'perc' => 'perc',
    ),
  );

  $items["field_sale_items:field_sale_items_discount_amount:field_collection_item:field_sale_items_discount_type:required:abs-perc"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items',
    'field_name' => 'field_sale_items_discount_amount',
    'control_field' => 'field_sale_items_discount_type',
    'state' => 'required',
    'condition_type' => 'or',
    'trigger_values' => array(
      'abs' => 'abs',
      'perc' => 'perc',
    ),
  );

  $items["field_sale_items_payment_mods:field_sale_payment_date:field_collection_item:field_sale_payment_due_with:required:dat"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items_payment_mods',
    'field_name' => 'field_sale_payment_date',
    'control_field' => 'field_sale_payment_due_with',
    'state' => 'required',
    'condition_type' => 'or',
    'trigger_values' => array(
      'dat' => 'dat',
    ),
  );

  $items["field_sale_items_payment_mods:field_sale_payment_date:field_collection_item:field_sale_payment_due_with:required:dat"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items_payment_mods',
    'field_name' => 'field_sale_payment_date',
    'control_field' => 'field_sale_payment_due_with',
    'state' => 'required',
    'condition_type' => 'or',
    'trigger_values' => array(
      'dat' => 'dat',
    ),
  );

  $items["field_sale_items_payment_mods:field_sale_payment_date:field_collection_item:field_sale_payment_due_with:visible:dat"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items_payment_mods',
    'field_name' => 'field_sale_payment_date',
    'control_field' => 'field_sale_payment_due_with',
    'state' => 'visible',
    'condition_type' => 'or',
    'trigger_values' => array(
      'dat' => 'dat',
    ),
  );

  $items["field_sale_items_payment_mods:field_sale_payment_date:field_collection_item:field_sale_payment_due_with:visible:dat"] = array(
    'type' => 'field_collection_item',
    'bundle' => 'field_sale_items_payment_mods',
    'field_name' => 'field_sale_payment_date',
    'control_field' => 'field_sale_payment_due_with',
    'state' => 'visible',
    'condition_type' => 'or',
    'trigger_values' => array(
      'dat' => 'dat',
    ),
  );

  return $items;
}
