<?php
/**
 * Includes functions for pdf generation related to sales
 */

/**
 * Creates a PDF from a sale and attaches it
 */
function _erpal_sale_sale_pdf($sale, $doctype, $attach = false, $print_ci = true) {
  
  $can_print = erpal_print_prepared();
  if (!$can_print)
    return false;

  $html = _erpal_sales_render_html($sale, $doctype);
  
  if($attach) {
    
    $params = array(
      'title' => $sale->title,
      'author' => _erpal_print_get_author($sale),
      'print_ci' => $print_ci,
    );
    
    $pdf_uri = _erpal_docs_folder_uri();
    $transaction_number = $sale->field_sale_transaction_number['und'][0]['value'];
    $ci_string = $print_ci ? '' : '-no_ci';

    $basename = _erpal_docs_make_filename($doctype . '-' . $transaction_number . $ci_string, 'pdf');    
    $pdf_file = _erpal_print_html_as_pdf($html, $pdf_uri, $basename, $params);
    
    $files = _erpal_sale_get_sale_docs($sale);
    $file = isset($files[$doctype]) ? $files[$doctype] : false;
        
    $erpal_file = _erpal_docs_attach_file($sale, $pdf_file, $file);
    
    _erpal_sales_file_add_tag($erpal_file, $doctype);
    
  }
  else {
    
    $download_filename = $sale->title.'_'.time().'.pdf';
    $params = array(
      'title' => $sale->title,
      'author' => _erpal_print_get_author($sale),
      'print_ci' => $print_ci,
    );
    _erpal_print_html_as_pdf($html, false, $download_filename, $params);
  } 
  
  return true;
}

 
/**
 * Returns a sale node rendered as html
 */
function _erpal_sales_render_html($sale_node, $doctype) {
    
  $result = theme('erpal_sales_pdf_html', array('sale' => $sale_node, 'doctype' => $doctype));
 
  return $result;
}

/**
 * Adds a tag (as string) to an erpal file
 * creates a new term if it doesn't exist
 */
function _erpal_sales_file_add_tag($erpal_file, $tag) {

  $term = taxonomy_get_term_by_name($tag, 'file_tags');
  
  if(empty($term)) {
    $vocab = taxonomy_vocabulary_machine_name_load('file_tags');
    $term = _erpal_sales_create_taxonomy_term($tag, $vocab->vid);   
  }

  node_object_prepare($erpal_file);
  $erpal_file->field_file_tags[LANGUAGE_NONE][0]['tid'] = array_keys($term);
  field_attach_update('node', $erpal_file);
}

/** 
 * returns an array of documents associated with the sale
 * keys are the tags, e.g. $result['offer'] will contain the offer document
 */
function _erpal_sale_get_sale_docs($sale_node) {
  
  $result = array();
  $sale = entity_metadata_wrapper('node', $sale_node);
  
  foreach($sale->field_asset_files->value() as $file_node) {
    $file = entity_metadata_wrapper('node', $file_node);
    foreach($file->field_file_tags->value() as $tag) {
      $result[$tag->name] = $file_node;
    }    
  }
  
  return $result;
}

/**
 * Create a taxonomy term and return the tid.
 * Taken from
 * https://api.drupal.org/comment/18799#comment-18799
 */
function _erpal_sales_create_taxonomy_term($name, $vid) {
  $term = new stdClass();
  $term->name = $name;
  $term->vid = $vid;
  taxonomy_term_save($term);
  return array($term->tid => taxonomy_term_load($term->tid));
}
