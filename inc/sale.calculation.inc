<?php
/***
 * Includes everything that handles calculation of data in the erpal_sales module
 */
 

/**
 * Calculates each of the Sale's items' totals with and without VAT and
 * prefills the fields in each sale item
 */
function _erpal_sales_calculate_item_totals($entity) {

  $wrapped_item = entity_metadata_wrapper('field_collection_item', $entity);      
  $quantity = $wrapped_item->field_sale_items_quantity->value();
  $singlePrice = $wrapped_item->field_sale_items_single_price->value();  
  $subtotal = $quantity * $singlePrice;
  
  //discount handling
  $discount_amount = $wrapped_item->field_sale_items_discount_amount->value();
  $discount_type = $wrapped_item->field_sale_items_discount_type->value();
  if($discount_type == 'abs')
    $subtotal -= $discount_amount;
  else if($discount_type == 'perc')
    $subtotal *= (1-($discount_amount/100));
  
  //VAT handling  
  $vatrate = 1.0;   
  $vat = $wrapped_item->field_vat_rate->value();
  $vatrate += ($vat/100);
  
  $wrapped_item->field_sale_items_subtotal_wo_vat = $subtotal;
  $wrapped_item->field_sale_items_subtotal_w_vat = $subtotal * $vatrate;
}

/**
 * Calculates the Sale's total by adding all the items' subtotals and sets the field
 * in the Sale
 */
function _erpal_sales_calculate_sale_total($entity) {

  $with_vat = $no_vat = .0;
  
  $node = entity_metadata_wrapper('node', $entity);
  foreach($node->field_sale_items->value() as $sale_item) {
    $wrapped_item = entity_metadata_wrapper('field_collection_item', $sale_item);
    $with_vat += $wrapped_item->field_sale_items_subtotal_w_vat->value();
    $no_vat += $wrapped_item->field_sale_items_subtotal_wo_vat->value();
  }  
  
  $node->field_sale_total_incl_vat = $with_vat;
  $node->field_total_excl_vat = $no_vat;
  if($node->field_activity_ref->value() != null) {
    $activity = $node->field_activity_ref->value();
    $activity->field_volume[LANGUAGE_NONE][0]['value'] = $no_vat;
    node_save($activity);
  }
}