<?php
/***
 *  Config file, handles admin menu for sales
 */

/**
 * Adds menu item for sales to the menu
 */
function _erpal_sales_config_menu() {
    
  $items['admin/erpal/sales'] = array(
    'title' => 'ERPAL Sale',
    'page callback' => '_erpal_sales_config_basic',
    'access arguments' => array('config erpal sale'),
    'file' => 'inc/config.inc',
    'type' => MENU_LOCAL_TASK,
  );
  
  return $items;
}

/**
 * Callback for _erpal_sales_config_menu
 */
function _erpal_sales_config_basic() {
  
  $form = drupal_get_form('erpal_sales_config_form');
  return $form;
}

/**
 * Creates the config form
 */
function erpal_sales_config_form($form, &$form_state) {
  
  $form = array();
  
  $form['erpal_sale_confirmation_string'] = array(
    '#type' => 'textfield',
    '#title' => t('Sale confirmation title'),
    '#description' => t('This text is the title of a confirmation type pdf document'),
    '#default_value' => _erpal_sales_get_confirmation_string(),
  );
  
  $form['erpal_sale_offer_string'] = array(
    '#type' => 'textfield',
    '#title' => t('Sale offer title'),
    '#description' => t('This text is the title of an offer type pdf document'),
    '#default_value' => _erpal_sales_get_offer_string(),
  );
  
  $form['erpal_sale_confirmation_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Sale confirmation text'),
    '#description' => t('This text is the text displayed in a confirmation type pdf document'),
    '#default_value' => _erpal_sales_get_confirmation_text(),
  );
  
  $form['erpal_sale_offer_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Sale offer text'),
    '#description' => t('This text is the text displayed in a offer type pdf document'),
    '#default_value' => _erpal_sales_get_offer_text(),
  );
  
  $form['transaction_number'] = array(
    '#type' => 'fieldset',
    '#title' => t('Transaction number'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  
  $form['transaction_number']['erpal_transaction_number_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction number pattern'),
    '#default_value' => _erpal_sales_get_transaction_number_pattern(),    
    '#description' => t('This pattern will let you design your transaction number'),
  );
  
  $form['transaction_number']['erpal_transaction_number_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction number offset'),
    '#default_value' => variable_get('erpal_last_transaction_number', 1),    
    '#description' => t('This offset definies current transaction number. Next transaction number will be this +1'),
  );
  
  //Tokens for transaction pattern
  //show available Tokens  
  $form['transaction_number']['token_tree'] = array(
    '#theme' => 'token_tree',   
    //'#token_types' => array(),
    '#global_types' => TRUE,
    '#click_insert' => TRUE,
    '#recursion_limit' => 1,
  );
  
  //fieldset for all company specific settings
  $form['skonto_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Skonto settings'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  
  $skonto_amount = _erpal_invoice_helper_get_skonto_rate();
  $skonto_duration = _erpal_invoice_helper_get_skonto_period();
  $skonto_string = t("Your default skonto is $skonto_amount% within a $skonto_duration day period.");
  
  //selected company
  $form['skonto_settings']['skonto'] = array(
    '#type' => 'item',
    '#title' => t('Skonto'),
    '#markup' => $skonto_string,
    '#description' => t('You can change your skonto in !link', array('!link' => l(t('ERPAL invoice settings'), 'admin/erpal/invoice'))),    
  );
  
  $form['submit'] = array(
    '#value' => t('save'),
    '#type' => 'submit',
    '#submit' => array('_erpal_sales_config_form_submit'),
  );
  
  return $form;
}

/**
 * Submithandler for the form
 */
function _erpal_sales_config_form_submit($form, $form_state) {
  
  $values = $form_state['values'];  
  
  foreach($values as $key=>$value) {
    //exceptions
    if($key == 'erpal_last_transaction_number'){
      variable_set('erpal_last_transaction_number', $values['erpal_transaction_number_offset']);
      continue;
    }
  
    variable_set($key, $value);
  }
}

function _erpal_sales_get_confirmation_string() {
  return variable_get('erpal_sale_confirmation_string', t('Order confirmation'));
}

function _erpal_sales_get_offer_string() {
  return variable_get('erpal_sale_offer_string', t('Offer'));
}

function _erpal_sales_get_confirmation_text() {
  $default = t('Thank you for your order. We will be providing you with an invoice containing the details for your payment shortly.');
  return variable_get('erpal_sale_confirmation_text', $default);
}

function _erpal_sales_get_offer_text() {
  $default = t('Thank you for considering us as a contractor.');
  return variable_get('erpal_sale_offer_text', $default);
}

function _erpal_sales_get_transaction_number_pattern() {
  $pattern = variable_get('erpal_transaction_number_pattern', '');
  
  if(!$pattern)
    $pattern = '[erpal_sales:next_transaction_number]';
  
  return $pattern;
}