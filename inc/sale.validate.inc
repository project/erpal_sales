<?php
/***
 * functions for the validation of sales and its items
 */

/**
 * Validates whether the modalities in a sale add up to the sale's total price
 */
function _erpal_sales_validate_sale_item($sale_item, $delta) {
  
  $mod_sum = .0;

  $sale_subsum = $sale_item['field_sale_items_quantity'][LANGUAGE_NONE][0]['value']['#value'] * 
                  $sale_item['field_sale_items_single_price'][LANGUAGE_NONE][0]['value']['#value'];
                  
  $discount = .0;
  $discount_type = isset($sale_item['field_sale_items_discount_type'][LANGUAGE_NONE]['#value']) ?
                    $sale_item['field_sale_items_discount_type'][LANGUAGE_NONE]['#value'] : 
                    '';
  
  if($discount_type == 'abs') {
    $discount = $sale_item['field_sale_items_discount_amount'][LANGUAGE_NONE][0]['value']['#value'];
  } else if ($discount_type == 'perc') {
    $discount = ($sale_item['field_sale_items_discount_amount'][LANGUAGE_NONE][0]['value']['#value'] / 100)*$sale_subsum;
  }
  
  $sale_subsum -= $discount;
                   
  foreach($sale_item['field_sale_items_payment_mods'][LANGUAGE_NONE] as $mod) {
    if(isset($mod['#entity_type']) && ($mod['#entity_type'] == 'field_collection_item')) {      
      $mod_sum += _erpal_sales_get_mod_sum($mod, $sale_subsum);
    }
  }
  
  //TODO: doesn't mark the wrong item
  if($mod_sum != $sale_subsum)
    form_set_error('field_sale_items_payment_mods', t("Selected payment modalities do not match the sale item's subtotal."));
}

/**
 * Returns the amount a modality is worth in the context of its sale item
 */
function _erpal_sales_get_mod_sum($mod, $sale_subsum) {

  $result = .0;
  
  $mod_type = $mod['field_sale_payment_price_type'][LANGUAGE_NONE]['#value'];
  $mod_amnt = $mod['field_sale_payment_amount'][LANGUAGE_NONE][0]['value']['#value'];  
  if($mod_type == 'perc') {
    $result += ($mod_amnt/100)*$sale_subsum;
  } 
  else if ($mod_type == 'abs') {
    $result += $mod_amnt;
  }
  
  return $result;
}