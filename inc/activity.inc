<?php

/**
* @file including all functions deadling with crm activities
*/

/**
* Updates the activity referenced by the sale node
* @param $sale_node node the sale node object that was changed
*/
function _erpal_sales_update_activity($sales_node) {
  
  //update only if the total price of the sales node changed compared to the previous version if there is one
  $original_sales_node = !empty($sales_node->original) ? $sales_node->original : false;
  
  $sales = entity_metadata_wrapper('node', $sales_node);
  $old_total = false;
  $old_activity = false;
  $new_total = $sales->field_total_excl_vat->value();
  if ($original_sales_node) {
    $original_sales = entity_metadata_wrapper('node', $original_sales_node);
    $old_total = $original_sales->field_total_excl_vat->value();
    $old_activity_node = $original_sales->field_activity_ref->value();
  }
  
  //the sales volume changed so update the activity volume
  $sale = entity_metadata_wrapper('node', $sales_node);
  $activity_node = $sale->field_activity_ref->value();
  
  if (empty($activity_node))
    return; //if there is no activity, nothing to update
  
  //if it didn't change or the activity didn't change, return
  if ($old_total == $new_total && $old_activity_node == $activity_node)
    return;
  
  $activity = entity_metadata_wrapper('node', $activity_node);
  
  //get the total of all sales referencing this activity
  //get all other sales nodes referencing this activity
  $sales_nids = _erpal_sales_get_sales_by_activity($activity_node);
  //get sum of all sales nids
  $sum = 0;
  foreach ($sales_nids as $sales_nid) {
    if ($sales_nid == $sales_node->nid) {
      $a_sales_node = $sales_node;
    } else {
      $a_sales_node = node_load($sales_nid);
    }
    
    $a_sales = entity_metadata_wrapper('node', $a_sales_node);
    $sum = $sum + $a_sales->field_total_excl_vat->value();
  }

  //set the sum to the activity
  $activity->field_volume->set($sum);
  $activity->save();
  
  //inform the user
  $activity_link = l($activity_node->title, 'node/'.$activity_node->nid);
  drupal_set_message(t('Updated the total volume of CRM activity !activity_link', array('!activity_link' => $activity_link)));
}

/**
* Returns the nids of all sales nodes referencing this activity
*/
function _erpal_sales_get_sales_by_activity($activity_node) {
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'erpal_sale')
    ->fieldCondition('field_activity_ref', 'target_id', $activity_node->nid)
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
  $result = $query->execute();
  $sales_nids = array();
  if (isset($result['node'])) {
    $sales_nids = array_keys($result['node']);
  }
  
  return $sales_nids;
}