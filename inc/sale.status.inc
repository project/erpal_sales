<?php
/***
 * contains functions to handle the status changing in a sale
 */
 
/**
 * Checks whether the Sale's status has been changed from submitted to accepted
 * If so, the sale's items are being appended to a new or an existing project as
 * tasks
 * @param $entity node the erpal_sale node where the status transition happens
 */
function _erpal_sales_handle_status_change($entity) {
  
  $new_node = entity_metadata_wrapper('node', $entity);
  $customer_nid = $new_node->field_customer_ref->value()->nid;
  $contractor_nid = $new_node->field_contractor_ref->value()->nid;

  $old_node = false;
  if(isset($entity->nid)) {
    $old_node = entity_metadata_wrapper('node', node_load($new_node->nid->value()));    
  }
    
  //if status is set to offer submitted
  if(($new_node->field_sale_status->value() == 'offer_accepted') && (($old_node && $old_node->field_sale_status->value() != 'offer_accepted') || !$old_node)) {
  
    $activity_data = $new_node->field_activity_ref->value();
    $activity = entity_metadata_wrapper('node', $activity_data);
    
    if($activity == null) 
      throw new Exception(t('No activity assigned to the sale.'));
     
    //FOREACH sale item
    foreach($new_node->field_sale_items as $sale_item) {
    
      //Project is only needed if a project is sold, otherwise, if the sales item type is "other" billables are created directly but not related to a project
      $sales_item_type = $sale_item->field_sale_items_item_type->value();
      $project = false;
      
      if ($sales_item_type == 'proj') {
        $project = _erpal_sales_sale_get_project($activity->nid->value());
        
        if($project == null) {        
          //Create the project
          $project = _erpal_sales_create_new_project($new_node, $activity);            
        }   
      
      } 
                  
      //FOREACH sale modality
      foreach($sale_item->field_sale_items_payment_mods as $modality) {        
        //create task for this position
        if ($project) {
          _erpal_sales_create_new_task($sale_item, $modality, $project);          
        } else {
          //create billables directly as we dont have a project sales item, and if modality is "ord" (for order) or "dat" for payment on a certain date, in this case the billable will have the delivery date according to this value
          $due_with = $modality->field_sale_payment_due_with->value();

          if ($due_with == 'ord' || $due_with == 'dat') {
            
            if ($due_with == 'ord')
              $date = time();
            if ($due_with == 'dat')
              $date = $modality->field_sale_payment_date->value();

            _erpal_sales_create_billables($sale_item, $modality, $date, $contractor_nid, $customer_nid);
          }
        }
      }
    }        
  } 

  //if status is set to "delivered" and there is a "other" typed sales item with a payment modality to pay after delivery, a billable is created. Also if a date is set, the billable is create with the given date. 
  if(($new_node->field_sale_status->value() == 'delivered') && (($old_node && $old_node->field_sale_status->value() != 'delivered') || !$old_node)) {
    //FOREACH sale item
    
    foreach($new_node->field_sale_items as $sale_item) {
    
      //If the sales item type is "other" billables are created directly but not related to a project
      $sales_item_type = $sale_item->field_sale_items_item_type->value();
      if ($sales_item_type == 'other') {
        
        //FOREACH sale modality
        foreach($sale_item->field_sale_items_payment_mods as $modality) {        
          //create task for this position
          $due_with = $modality->field_sale_payment_due_with->value();
          if ($due_with == 'del') {  //billable by delivery
            _erpal_sales_create_billables($sale_item, $modality, time(), $contractor_nid, $customer_nid);
          }
        }
      }
    }        
  }

}

/**
* Creates direct billables for a sales item
* @param $sale_item field_collection_item this is the sale item we need to create a billable for
* @param $date the deliverydate of the billable
*/
function _erpal_sales_create_billables($sale_item, $modality, $date, $contractor_nid, $customer_nid) {
  
  //create the billable
  $billable = entity_create('billable', array());
  $billable->subject = _erpal_sales_get_description($sale_item, $modality);
  $billable->currency = $sale_item->field_currency->value();
  $billable->subject_nid = 0; //no subject
  $billable->quantity = 1; //quantity is always 1 @TODO may be change this if we need the amount printed in the invoice
  $billable->vat_rate = $sale_item->field_vat_rate->value();
  $billable->date_delivery = is_integer($date) ? $date : strtotime($date);
  $billable->customer_nid = $customer_nid;  
  $billable->contractor_nid = $contractor_nid;  
  
  if ($billable)  
    $billable->quantity = 1;
    
  $price = _erpal_sale_get_modality_price($modality, $sale_item);
  $billable->single_price = $price;
  
  $billable->save();
  return $billable;
}

/**
 * Returns the project associated with an activity or null if it doesn't exist 
 */
function _erpal_sales_sale_get_project($activity_nid) {
  
  $result = null;
  
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'erpal_project')
    ->fieldCondition('field_activity_ref', 'target_id', $activity_nid, '=')
    ->range(0, 1)
    ->addMetaData('account', user_load(1)); //run as admin?  - TEST if this is neccessary    
  $t_result = $query->execute();
  
  if(isset($t_result['node'])) {
    $p_nids = array_keys($t_result['node']);
    $result = entity_metadata_wrapper('node', node_load($p_nids[0]));
  }  
  return $result;
}

/**
 * Creates and returns a new project for a sale and attaches an activity to it.
 */
function _erpal_sales_create_new_project($node, $activity) {
  
  global $user;
  $entity = entity_create('node', array('type' => 'erpal_project'));
  $entity->uid = $user->uid;
  $wrapped_project = entity_metadata_wrapper('node', $entity);
  
  $wrapped_project->title = 'Project for '.$node->title->value();
  $vid = _erpal_basic_helper_term_field_get_vid('field_project_status_term'); 
  $wrapped_project->field_project_status_term = _erpal_basic_helper_get_default_tid($vid); 
  $wrapped_project->field_activity_ref = $activity;
  $wrapped_project->field_customer_ref = $node->field_customer_ref->value();

  //create a pricing field collection entity, even if it is empty
  $pricing_fci = entity_create('field_collection_item', array('field_name' => 'field_pricing'));    
  $pricing_fci->setHostEntity('node', $entity);
  $pricing_fci->save();
  
  $wrapped_project->save();

  return $entity;
}

/**
* Returns the title or subject that will be set for a task or a billable according to the description of either the modality or the sale item
*/
function _erpal_sales_get_description($sale_item, $modality) {
  $desc = $modality->field_sale_payment_description->value();
  $sales_item_desc = $sale_item->field_sale_items_description->value();
  
  return !empty($desc) ? $desc : $sales_item_desc;
}

/**
 * Creates a new Task from a sale item and its modalities and attaches it to a project.
 */
function _erpal_sales_create_new_task($sale_item, $modality, $project) {
  
  global $user;
 
  $node = entity_create('node', array('type' => 'erpal_task'));
 
  $wrapped_task = entity_metadata_wrapper('node', $node);
   
  //Use description of sale item if any, else use products description
  $node->title = _erpal_sales_get_description($sale_item, $modality); 
     
  $sale_item_string = ''; //no task description
 
  $wrapped_task->body = array('value' => $sale_item_string,
    'summary' => '',
    'format' => 'full_html');  
  $wrapped_task->field_project_ref = $project;
  //set the default status
  
  
  $pricing_fci = entity_create('field_collection_item', array('field_name' => 'field_pricing'));  
  $wrapped_pricing = entity_metadata_wrapper('field_collection_item', $pricing_fci);
  $wrapped_pricing->field_price_mode = 'fixed_price';
  $wrapped_pricing->field_vat_rate = $sale_item->field_vat_rate->value();
  $wrapped_pricing->field_currency = $sale_item->field_currency->value();
  
  //get the price of the modality
  $t_price = _erpal_sale_get_modality_price($modality, $sale_item);
  
  $wrapped_pricing->field_price = $t_price;
  
  _erpal_sale_set_task_default_values($node);
  
  $pricing_fci->setHostEntity('node', $node);
  $wrapped_pricing->save(true);
  
  switch ($modality->field_sale_payment_due_with->value()) {
    case 'ord':
      $wrapped_task->field_task_status_term =  _erpal_sales_get_task_completed_term();
      break;
    case 'dat':
      //this needs to be changed when task gets a unix timestamp
      $node->field_date[LANGUAGE_NONE][0]['value'] = date('Y-m-d', $modality->field_sale_payment_date->value());
      break;
  }
    
  $wrapped_task->save();
}

/**
* Sets the default status of a task
*/ 
function _erpal_sale_set_task_default_values($task_node) {
  $vid = _erpal_basic_helper_term_field_get_vid('field_task_status_term');
  $default_tid = _erpal_basic_helper_get_default_tid($vid);
  $task_node->field_task_status_term[LANGUAGE_NONE][0]['tid'] = $default_tid;
  $task_node->field_progress_percent[LANGUAGE_NONE][0]['value'] = 0;
}

/**
* Get the price of the sales item according to the modality
*/
function _erpal_sale_get_modality_price($modality, $sale_item) {
  
  $t_price = .0;
  if($modality->field_sale_payment_price_type->value() == 'abs') {    
    $t_price = $modality->field_sale_payment_amount->value();
  }
  else if($modality->field_sale_payment_price_type->value() == 'perc') {
    $t_price = ($modality->field_sale_payment_amount->value() / 100) * $sale_item->field_sale_items_subtotal_wo_vat->value();
  }
  else {
    $t_price = $sale_item->field_sale_items_subtotal_wo_vat->value();
  }
  
  return $t_price;
}


/**
 * Returns the first taxonomy term that has the semantic meaning "completed"
 */
function _erpal_sales_get_task_completed_term() {

  $result = null;
  
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'taxonomy_term')
    ->entityCondition('bundle', 'task_status_terms')
    ->fieldCondition('field_simple_process_status', 'value', 'completed', '=')
    ->range(0, 1);
  $t_result = $query->execute();
  
  if(isset($t_result['taxonomy_term'])) {  
    $t_ids = array_keys($t_result['taxonomy_term']);
    $result = taxonomy_term_load($t_ids[0]);
  }
  else throw new Exception(t('No task status with semantic meaning "completed" found.'));
  
  return $result;     
}

