<?php
/***
* functions for everything that deals with tokens for erpal sales
*/

/**
 * Implements hook_token_info()
 */
function erpal_sales_helper_token_info() {

  $types['erpal_sales'] = array(
    'names' => t('Erpal sales token'),
    'description' => t('Tokens for sales notes'),
    'needs-data' => 'erpal_sales',
  );
  
  $sale['next_transaction_number'] = array(
    'name' => t('Next transaction number'),
    'description' => t('The next transaction number in the system'),
  );
  
  return array(
    'types' => $types,
    'tokens' => array(
      'erpal_sales' => $sale,
    ),
  );
}

/**
 * Implements hook_tokens()
 */
function erpal_sales_tokens($type, $tokens, array $data = array(), array $options = array()) {
    
  $replacements = array();
  
  if($type == 'erpal_sales') {
    $sale = isset($data['erpal_sales']) ? $data['erpal_sales'] : false;
    
    foreach($tokens as $name => $original) {
      switch($name) {
        case 'next_transaction_number':
          $t_num = isset($data['transaction_number']) && $data['transaction_number'] ? 
            $data['transaction_number'] :
            _erpal_sales_get_next_transaction_number();
          $replacements[$original] = $t_num;          
          break;
      }
    }
  }
  else if ($type = 'erpal_invoice_skonto') {
    
    $sale = isset($data['erpal_sale']) ? $data['erpal_sale'] : false;
    if($sale) {
      $skonto = _erpal_sales_get_skonto($sale);
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'skonto_period':
            $replacements[$original] = $skonto['period'];
          break;
          case 'skonto_rate':
            $replacements[$original] = $skonto['rate'];
          break;               
        }
      }
    }
  } 
  return $replacements;
}