<?php 
/*
* Available variables:
* $company
* $customer
* $transaction_number
* $valid_until
* $document_date
* $sale_items
* $sale
* $doctype
* $skonto_text
*/
?>
<link rel="stylesheet" type="text/css" href="<?php print drupal_get_path('module', 'erpal_sales'); ?>/template/common.css">
<table>
  <tr><td style="height:1.5cm;">&nbsp;</td></tr>
</table>
<table id="head_table">
  <tr>
    <td>
      <div id="sender">
        <?php if ($company['name']) {print $company['name']; print "&nbsp;-";} ?>
        <?php if ($company['street']) {print $company['street']; print "&nbsp;-";} ?>
        <?php if ($company['zip']) {print $company['zip'];} ?>&nbsp;<?php if ($company['city']) {print $company['city'];} ?>
      </div>
      <div id="recipient">
        <?php if ($customer['name']) {print $customer['name']; print "<br />";} ?>
        <?php if ($customer['address_additionals']) {print "&nbsp;&nbsp;"; print $customer['address_additionals']; print "<br />";} ?>        
        <?php if ($customer['street']) {print "&nbsp;&nbsp;"; print $customer['street']; print "<br />";} ?>
        <?php if ($customer['zip']) {print "&nbsp;&nbsp;"; print $customer['zip'];} ?>&nbsp;<?php if ($customer['city']) {print $customer['city']; print "<br />";} ?>
        <?php if ($customer['country']) {print "&nbsp;&nbsp;"; print $customer['country']; print "<br />";} ?>        
      </div>
    </td>      
  </tr>
  <tr>
    <td>      
      <table>
        <tr><td style="height:1.5cm;">&nbsp;</td></tr>
      </table>
      <table id="receipt_data">
        <tr>
          <td id="transaction_info" class="left">
            <table>
              <tr>
                <td><?php print ('<span class="doctype">'.$doctype.'</span>'); ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td style="height:1cm">&nbsp;</td>
              </tr>
              <tr>
                <td><?php print t('Transaction Nr. !trans_nr', array('!trans_nr' => $transaction_number)); ?></td>                
              </tr>
              <tr>
                <td><?php if ($customer['vat_number']) {print t('VAT number').':&nbsp;'; print $customer['vat_number']; print "<br />";} ?></td>
              </tr>
            </table>  
          </td>
          <td id="receipt_date" class="right">
            <table>              
              <tr>
                <td><?php print (t('Date').": $document_date"); ?></td>
              </tr>
            </table>            
          </td>      
        </tr>   
      </table>
    </td>
  </tr>
</table>

<table>
  <tr><td style="height:0.8cm;">&nbsp;</td></tr>
</table>
<?php
$table = array();

$data['header'] = array(
    'artid' => array('data' => t("Art. Ident."), "class" => "left artid"),
    'amount' => array('data' => t("Quantity"), "class" => "left quantity"),
    'description' => array('data' => t("Item desc."), "class" => "left description"),
    'price' => array('data' => t("Single price"), "class" => "left price"),
    'discount' => array('data' => t("Discount"), "class" => "right discount"),
    'vat' => array('data' => t("VAT rate"), "class" => "right price"),
    'total' => array('data' => t("Total"), "class" => "right total"),
    //'currency' => array('data' => t("Currency"), "class" => "left currency"),
);

if(is_array($sale_items)) {
  
  $rows = array();
  
  foreach($sale_items as $i=>$sale_item) {
    
    $row = array();
    
    //artident    
    $row[] = array('data' => $sale_item['artid'], 'class' => 'left');
    
    //quantity
    $row[] = array('data' => $sale_item['quantity'],
      'class' => 'left quantity');
      
    //description   
    $row[] = array('data' => $sale_item['description'], 'class' => 'left description');
    
    //price    
    $row[] = array('data' => number_format($sale_item['single_price'], 2, ',', '.').' '.$sale_item['currency'],
      'class' => 'right');
    
    //discount
    switch($sale_item['discount_type']) {
      case 'perc':
        $row[] = array('data' => $sale_item['discount_amount'].'%', 'class' => 'right');
        break;
        
      case 'abs':
        $row[] = array('data' => number_format($sale_item['discount_amount'], 2, ',', '.').' '.$sale_item['currency'],
        'class' => 'right');
        break;
      
      default:
        $row[] = array();
    }
        
    //vat rate        
    $row[] = array('data' => number_format($sale_item['vat'], 0)."%", 'class' => 'right');
    
    //subtotal    
    $row[] = array('data' => number_format($sale_item['subtotal'], 2, ',', '.').' '.$sale_item['currency'],
      'class' => 'right');
      
    //currency
    //$row[] = array('data' => $sale_item['currency'], 'class' => 'left');
    
    $rows[] = $row;
            
    $rows[] = array('data' => array(
        0 => '',
        1 => t('Amount'), 
        2 => t('Due with'),
        3 => t('Description'),
        4 => '',
        5 => '',
        6 => '',
      ),
      'class' => array('small miniheader'),
    );
    foreach($sale_item['modalities'] as $j=>$modality) {
      $row = array(0 => '');
      
      //amount
      switch($modality['type']) {
        case 'perc':
          $row[] = array('data' => $modality['amount'].'%', 'class' => 'small left');
          break;
          
        case 'abs':
          $row[] = array('data' => number_format($modality['amount'], 2, ',', '.').' '.$sale_item['currency'],
          'class' => 'small right');
          break;
      }
      
      //due with
      switch($modality['due']) {
        case 'ord':
          $row[] = array('data' => 'Order', 'class' => 'small left');
          break;
          
        case 'del':
          $row[] = array('data' => 'Delivery', 'class' => 'small left');
          break;
          
        case 'dat':
          $row[] = array('data' => $modality['due_date'], 'class' => 'small left');
          break;
      }
      
      //description
      $len = count($sale_items) - 1;
      $desc = $modality['description'];
      if($i != $len) {      
        $len = count($sale_item['modalities']) - 1;
        $desc = $j == $len ? $desc.'<br />' : $desc;
      }      
      
      $row[] = array('data' => $desc, 
        'class' => 'small left',
        'colspan' => 4,
      );
      
      $rows[] = $row;
    }
  }
  
  //total excl vat
  $total_excl_vat = $sale->field_total_excl_vat['und'][0]['value'];
  $total_incl_vat = $sale->field_sale_total_incl_vat['und'][0]['value'];
  
  $rows[] = array('data' => array(
        0 => '',
        1 => '',
        2 => '',
        3 => '',
        4 => array('data' => t("Total excl. VAT"), 'class' => 'right', 'colspan' => 2),
        5 => array('data' => number_format($total_excl_vat, 2, ',', '.').' '.$sale_item['currency'], 'class' => 'right'),
    ),
    'class' => array('sumrow', 'bordertop'),
  );
    
  //vat only
  $rows[] = array('data' => array(
          0 => '',
          1 => '',
          2 => '',
          3 => '',
          4 => array('data' => t("VAT"), 'class' => 'right', 'colspan' => 2),
          5 => array('data' => number_format(($total_incl_vat-$total_excl_vat), 2, ',', '.').' '.$sale_item['currency'], 'class' => 'right'),
      ),
      'class' => array('sumrow'),
  );
  
  //total incl vat
  $rows[] = array('data' => array(
          0 => '',
          1 => '',
          2 => '',
          3 => '',
          4 => array('data' => t("Total incl. VAT"), 'class' => 'right', 'colspan' => 2),
          5 => array('data' => number_format($total_incl_vat, 2, ',', '.').' '.$sale_item['currency'], 'class' => 'right'),
      ),
      'class' => array('sumrow'),
  );
  
  $data['rows'] = $rows;

  $data['attributes']['class'][] = 'receipt_table';

  $table = theme("table", $data);
  print $table; 
}
?>
<div id="receipt_notes">
  <?php 
    print "<br />".nl2br($text);
  ?>
</div>
<?php
if(isset($skonto_text)){
print '<div id="skonto_text"><br />'.nl2br($skonto_text).'</div>';
}
?>
</div>