<?php
/**
 * @file
 * erpal_sales.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function erpal_sales_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-erpal-menu:sales
  $menu_links['menu-erpal-menu:sales'] = array(
    'menu_name' => 'menu-erpal-menu',
    'link_path' => 'sales',
    'router_path' => 'sales',
    'link_title' => 'Sales',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => 'erpal_menu_sales',
        'class' => 'sales',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -39,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Sales');


  return $menu_links;
}
