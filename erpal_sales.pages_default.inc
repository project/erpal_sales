<?php
/**
 * @file
 * erpal_sales.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function erpal_sales_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_14';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 13;
  $handler->conf = array(
    'title' => 'Sale Node Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_sale' => 'erpal_sale',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'erpal_content_layout';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'bottom' => NULL,
      'sidebar_first_left' => NULL,
      'sidebar_first_right' => NULL,
      'sidebar_second_left' => NULL,
      'sidebar_second_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'bc0dae9e-f76a-f5d4-6db9-16a86bfd41fc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3c3efaed-b541-b024-3150-7fc1c5542ba3';
    $pane->panel = 'sidebar_first_right';
    $pane->type = 'page_actions';
    $pane->subtype = 'page_actions';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3c3efaed-b541-b024-3150-7fc1c5542ba3';
    $display->content['new-3c3efaed-b541-b024-3150-7fc1c5542ba3'] = $pane;
    $display->panels['sidebar_first_right'][0] = 'new-3c3efaed-b541-b024-3150-7fc1c5542ba3';
    $pane = new stdClass();
    $pane->pid = 'new-3b94da8d-9d11-9b94-25a8-cb6481a40b60';
    $pane->panel = 'sidebar_second_left';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3b94da8d-9d11-9b94-25a8-cb6481a40b60';
    $display->content['new-3b94da8d-9d11-9b94-25a8-cb6481a40b60'] = $pane;
    $display->panels['sidebar_second_left'][0] = 'new-3b94da8d-9d11-9b94-25a8-cb6481a40b60';
    $pane = new stdClass();
    $pane->pid = 'new-98c9e6bb-e777-1084-9d4f-0b445d603464';
    $pane->panel = 'sidebar_second_right';
    $pane->type = 'custom_node_content';
    $pane->subtype = 'custom_node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'types' => array(),
      'field_name' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '98c9e6bb-e777-1084-9d4f-0b445d603464';
    $display->content['new-98c9e6bb-e777-1084-9d4f-0b445d603464'] = $pane;
    $display->panels['sidebar_second_right'][0] = 'new-98c9e6bb-e777-1084-9d4f-0b445d603464';
    $pane = new stdClass();
    $pane->pid = 'new-82ccf189-71d2-40a4-0d4d-696ae16542cb';
    $pane->panel = 'sidebar_second_right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_asset_files';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'entityreference_file_node',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'link' => 1,
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'pane_collapsed' => 0,
      ),
      'style' => 'collapsible',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '82ccf189-71d2-40a4-0d4d-696ae16542cb';
    $display->content['new-82ccf189-71d2-40a4-0d4d-696ae16542cb'] = $pane;
    $display->panels['sidebar_second_right'][1] = 'new-82ccf189-71d2-40a4-0d4d-696ae16542cb';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_14'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function erpal_sales_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'sales';
  $page->task = 'page';
  $page->admin_title = 'Sales';
  $page->admin_description = '';
  $page->path = 'sales';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'access sales view',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_sales_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'sales';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Sales panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'erpal_content_layout';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar_first_left' => NULL,
      'sidebar_first_right' => NULL,
      'sidebar_second_left' => NULL,
      'sidebar_second_right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Sales';
  $display->uuid = 'd2c87940-fdbe-b904-41ce-58ee451647e3';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-fad8346a-11e1-2d44-adbf-ae26424b17a6';
    $pane->panel = 'sidebar_first_right';
    $pane->type = 'page_actions';
    $pane->subtype = 'page_actions';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fad8346a-11e1-2d44-adbf-ae26424b17a6';
    $display->content['new-fad8346a-11e1-2d44-adbf-ae26424b17a6'] = $pane;
    $display->panels['sidebar_first_right'][0] = 'new-fad8346a-11e1-2d44-adbf-ae26424b17a6';
    $pane = new stdClass();
    $pane->pid = 'new-6b1f6364-e29a-b174-bdcb-2667c3fc3cbd';
    $pane->panel = 'sidebar_second_left';
    $pane->type = 'views_panes';
    $pane->subtype = 'sales_view-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6b1f6364-e29a-b174-bdcb-2667c3fc3cbd';
    $display->content['new-6b1f6364-e29a-b174-bdcb-2667c3fc3cbd'] = $pane;
    $display->panels['sidebar_second_left'][0] = 'new-6b1f6364-e29a-b174-bdcb-2667c3fc3cbd';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-6b1f6364-e29a-b174-bdcb-2667c3fc3cbd';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['sales'] = $page;

  return $pages;

}
