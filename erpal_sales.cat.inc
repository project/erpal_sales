<?php
/**
 * @file
 * erpal_sales.cat.inc
 */

/**
 * Implements hook_cat_items_settings_info().
 */
function erpal_sales_cat_items_settings_info() {
  $export = array();

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_activities_actvity_view_projects_clone_clone';
  $cat_item->name = 'Erpal activities: actvity view: sales';
  $cat_item->catch_subpaths = 0;
  $cat_item->category = 'Activities';
  $cat_item->path = 'node/%';
  $cat_item->data = array(
    'api_version' => 1,
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_crm_activity' => 'erpal_crm_activity',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'active_trail_path' => 'activities',
    'arguments' => array(
      1 => array(
        'argument_name' => 'node',
        'argument_type' => 'path',
        'position' => 1,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'name' => 'Erpal activities: actvity view: sales',
    'path' => 'node/%',
    'category' => 'Activities',
    'cat_actions' => array(
      0 => array(
        'path' => 'node/add/erpal-sale',
        'path_router' => 'node/add/erpal-sale',
        'path_original' => 'node/add/erpal-sale',
        'path_pattern' => 'node/%/%',
        'path_query' => array(
          'field_activity_ref' => '[node:nid]',
        ),
        'path_destination' => FALSE,
        'title' => 'Create a new offer',
        'css_id' => '',
        'css_class' => '',
        'active' => TRUE,
      ),
    ),
    'cat_tabs' => array(),
    'machine_name' => 'erpal_activities_actvity_view_projects_clone_clone',
    'path_original' => 'node/%node',
    'path_pattern' => 'node/%',
    'path_query' => array(),
    'path_router' => 'node/%',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = -89;
  $export['erpal_activities_actvity_view_projects_clone_clone'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_sale_sale_edit';
  $cat_item->name = 'Erpal sale: sale edit';
  $cat_item->catch_subpaths = 0;
  $cat_item->category = 'Sales';
  $cat_item->path = 'node/%/edit';
  $cat_item->data = array(
    'api_version' => 1,
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_sale' => 'erpal_sale',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'active_trail_path' => 'sales',
    'arguments' => array(
      1 => array(
        'argument_name' => 'node',
        'argument_type' => 'path',
        'position' => 1,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'name' => 'Erpal sale: sale edit',
    'path' => 'node/%/edit',
    'category' => 'Sales',
    'cat_actions' => array(),
    'cat_tabs' => array(),
    'machine_name' => 'erpal_sale_sale_edit',
    'path_original' => 'node/%node/edit',
    'path_pattern' => 'node/%/%',
    'path_query' => array(),
    'path_router' => 'node/%/edit',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_sale_sale_edit'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_sale_sale_view';
  $cat_item->name = 'Erpal sale: sale view';
  $cat_item->catch_subpaths = 0;
  $cat_item->category = 'Sales';
  $cat_item->path = 'node/%';
  $cat_item->data = array(
    'api_version' => 1,
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'erpal_sale' => 'erpal_sale',
            ),
          ),
          'context' => 'node',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'active_trail_path' => 'sales',
    'arguments' => array(
      1 => array(
        'argument_name' => 'node',
        'argument_type' => 'path',
        'position' => 1,
        'argument' => 'entity_id:node',
        'settings' => array(
          'identifier' => 'Node: ID',
        ),
      ),
    ),
    'name' => 'Erpal sale: sale view',
    'path' => 'node/%',
    'category' => 'Sales',
    'cat_actions' => array(
      0 => array(
        'path' => 'node/[node:nid]/edit',
        'path_router' => 'node/[node:nid]/edit',
        'path_original' => 'node/[node:nid]/edit',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Edit this sale',
        'css_id' => '',
        'css_class' => '',
        'active' => TRUE,
      ),
    ),
    'cat_tabs' => array(
      0 => array(
        'path' => 'dashboard',
        'path_router' => 'dashboard',
        'path_original' => 'dashboard',
        'path_pattern' => 'dashboard',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Home',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_home|erpal_home',
      ),
    ),
    'machine_name' => 'erpal_sale_sale_view',
    'path_original' => 'node/%node',
    'path_pattern' => 'node/%',
    'path_query' => array(),
    'path_router' => 'node/%',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_sale_sale_view'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_sales_create_sale';
  $cat_item->name = 'Erpal Sales: Create Sales';
  $cat_item->catch_subpaths = 0;
  $cat_item->category = 'Sales';
  $cat_item->path = 'node/add/erpal-sale';
  $cat_item->data = array(
    'api_version' => 1,
    'access' => array(),
    'active_trail_path' => 'sales',
    'arguments' => array(),
    'name' => 'Erpal Sales: Create Sales',
    'path' => 'node/add/erpal-sale',
    'category' => 'Sales',
    'cat_actions' => array(),
    'cat_tabs' => array(
      0 => array(
        'path' => 'dashboard',
        'path_router' => 'dashboard',
        'path_original' => 'dashboard',
        'path_pattern' => 'dashboard',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Home',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
        'reusable' => FALSE,
        'machine_name' => '',
        'reusabled' => TRUE,
        'reusabled_machine_name' => 'erpal_home|erpal_home',
      ),
    ),
    'machine_name' => 'erpal_sales_create_sale',
    'path_original' => 'node/add/erpal-sale',
    'path_pattern' => 'node/%/%',
    'path_query' => array(),
    'path_router' => 'node/add/erpal-sale',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = -90;
  $export['erpal_sales_create_sale'] = $cat_item;

  $cat_item = new stdClass();
  $cat_item->api_version = 1;
  $cat_item->machine_name = 'erpal_sales_sales_list';
  $cat_item->name = 'Erpal sales: sales list';
  $cat_item->catch_subpaths = 0;
  $cat_item->category = 'Sale';
  $cat_item->path = 'sales';
  $cat_item->data = array(
    'api_version' => 1,
    'access' => array(),
    'active_trail_path' => 'sales',
    'arguments' => array(),
    'name' => 'Erpal sales: sales list',
    'path' => 'sales',
    'category' => 'Sale',
    'cat_actions' => array(
      0 => array(
        'path' => 'node/add/erpal-sale',
        'path_router' => 'node/add/erpal-sale',
        'path_original' => 'node/add/erpal-sale',
        'path_pattern' => 'node/%/%',
        'path_query' => array(),
        'path_destination' => FALSE,
        'title' => 'Creat a new offer',
        'css_id' => '',
        'css_class' => '',
        'active' => FALSE,
      ),
    ),
    'cat_tabs' => array(),
    'machine_name' => 'erpal_sales_sales_list',
    'path_original' => 'sales',
    'path_pattern' => 'sales',
    'path_query' => array(),
    'path_router' => 'sales',
    'reusable' => 0,
    'cat_remove' => array(),
  );
  $cat_item->weight = 0;
  $export['erpal_sales_sales_list'] = $cat_item;

  return $export;
}
