<?php
/**
 * @file
 * erpal_sales.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function erpal_sales_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_base
  $overrides["field_base.field_invoice_vat.cardinality"] = 1;
  $overrides["field_base.field_invoice_vat.settings|profile2_private"] = FALSE;

 return $overrides;
}
