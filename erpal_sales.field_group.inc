<?php
/**
 * @file
 * erpal_sales.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function erpal_sales_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_item_div|field_collection_item|field_sale_items|form';
  $field_group->group_name = 'group_item_div';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_sale_items';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Item',
    'weight' => '0',
    'children' => array(
      0 => 'field_sale_items_description',
      1 => 'field_sale_items_discount_amount',
      2 => 'field_sale_items_discount_type',
      3 => 'field_sale_items_item_type',
      4 => 'field_sale_items_payment_mods',
      5 => 'field_sale_items_product_ref',
      6 => 'field_sale_items_quantity',
      7 => 'field_sale_items_single_price',
      8 => 'field_sale_items_subtotal_w_vat',
      9 => 'field_sale_items_subtotal_wo_vat',
      10 => 'field_service_category_term',
      11 => 'field_currency',
      12 => 'field_vat_rate',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_item_div|field_collection_item|field_sale_items|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sale_pay_mods|field_collection_item|field_sale_items_payment_mods|form';
  $field_group->group_name = 'group_sale_pay_mods';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_sale_items_payment_mods';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Modality',
    'weight' => '0',
    'children' => array(
      0 => 'field_sale_payment_amount',
      1 => 'field_sale_payment_due_with',
      2 => 'field_sale_payment_price_type',
      3 => 'field_sale_payment_date',
      4 => 'field_sale_payment_description',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Modality',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-sale-pay-mods-fs',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_sale_pay_mods|field_collection_item|field_sale_items_payment_mods|form'] = $field_group;

  return $export;
}
